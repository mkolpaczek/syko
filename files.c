/*
 * files.c
 *
 *  Created on: 27 maj 2018
 *      Author: CEM
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "memory.h"
#include "files.h"

//Linux compliance
#ifndef O_BINARY
    #define O_BINARY 0 
#endif

#ifndef O_RDWR
	#define O_RDWR 0
#endif


void LoadProgramMemory(const char *file){          //�adowanie pami�ci kodu z pliku 
    int file_pointer;
    file_pointer = open(file, O_RDWR | O_BINARY, 0);
    if(file_pointer < 0) {
        printf("Code memory file not found! Correct file name is (%s)!\n", file);
        exit(-3);
    }
    lseek(file_pointer, 0, SEEK_SET);
    printf("Read program memory (%s) file in %dbytes\n", file, read(file_pointer, (void*)program_memory, PROGRAM_MEMORY_SIZE + 1));
    close(file_pointer);
}

void LoadDataMemory(const char *file){        //�adowanie pami�ci danych z pliku
    int file_pointer;
    file_pointer = open(file, O_RDWR | O_BINARY, 0);
    if(file_pointer < 0) {
        printf("Data memory file not found! Correct file name is (%s)!\n", file);
        exit(-4);
    }    

    lseek(file_pointer, 0, SEEK_SET);
    printf("Read data memory (%s) file in %dbytes\n", file, read(file_pointer, (void*)data_memory, DATA_MEMORY_SIZE + 1));
    close(file_pointer);
}

void SaveDataMemory(const char *file){        //zapisz zawarto�� pami�ci danych do pliku 
    int file_pointer;
    file_pointer = open(file, O_RDWR | O_BINARY, 0);
    if(file_pointer < 0){
        printf("Cannot open (%s) file!\n", file);
        exit(-6);
    }    
    lseek(file_pointer, 0, SEEK_SET);
    printf("Saved data memory file (%s) in %dbytes\n", file, write(file_pointer, (void*)data_memory, DATA_MEMORY_SIZE));
    close(file_pointer);
}

