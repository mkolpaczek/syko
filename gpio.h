#ifndef __GPIO_H__
#define __GPIO_H__


#include <stdint.h>
#include <stdbool.h>

typedef struct GpioEvent {
	uint16_t time_;
	bool new_state_;
} GpioEvent;

void LoadGpioChanges(const char *file);
bool GetGpioChange(uint16_t step_number);


#endif //__GPIO_H__
