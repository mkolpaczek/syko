/*
 * instructions.c
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */

#include "memory.h"


void Brne(int16_t argument_1) {
    printf("BRNE 0x%04X \n", argument_1);
	if(!GetBit(SREG, Z_BIT)) {
		program_counter = program_counter + argument_1 + 1;
	} else {
		program_counter = program_counter + 1;
	}
}

void Brge(int16_t argument_1) {
    printf("BRGE 0x%04X \n", argument_1);
	if( !((GetBit(SREG, N_BIT) ^ GetBit(SREG, V_BIT))) ) {
		program_counter = program_counter + argument_1 + 1;
	} else {
		program_counter = program_counter + 1;
	}
}

void Brsh(int16_t argument_1) {
    printf("BRSH 0x%04X \n", argument_1);
	if(!GetBit(SREG, C_BIT)) {
		program_counter = program_counter + argument_1 + 1;
	} else {
		program_counter = program_counter + 1;
	}
}

void Breq(int16_t argument_1) {
    printf("BREQ 0x%04X \n", argument_1);
	if(GetBit(SREG, Z_BIT)) {
		program_counter = program_counter + argument_1 + 1;
	} else {
		program_counter = program_counter + 1;
	}
}

void Ldi(uint16_t argument_1, uint16_t argument_2) {
    printf("LDI R%d, 0x%04X \n", argument_1, argument_2);
    registers[argument_1] = argument_2;
    program_counter++;
}

void Rcall(int16_t argument_1) {
	printf("RCALL 0x%04X \n", argument_1);
	data_memory[stack_pointer--] = ((program_counter + 1) >> 8);
	data_memory[stack_pointer--] = program_counter + 1;
	program_counter = program_counter + argument_1 + 1;

}

void Out(uint16_t argument_1, uint16_t argument_2) {
    printf("OUT $%d, R%d \n", argument_1, argument_2);
	in_out_memory[argument_1] = registers[argument_2];
	program_counter++;
}

void Andi(uint16_t argument_1, uint16_t argument_2) {
    printf("ANDI R%d, 0x%04X \n", argument_1, argument_2);
	uint8_t result = registers[argument_1] & argument_2;

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	ClearBit(SREG, ~V_BIT);
		// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Rjmp(int16_t argument_1) {
    printf("RJMP 0x%04X \n", argument_1);
	program_counter = program_counter + argument_1 + 1;
}

void Mul(uint16_t argument_1, uint16_t argument_2) {
    printf("MUL R%d, R%d \n", argument_1, argument_2);
	uint16_t result = registers[argument_1] * registers[argument_2];

	// carry flag
	if (result & (1 << 15)) {
		SetBit(SREG, C_BIT);
	} else {
		ClearBit(SREG, C_BIT);
	}

	// zero flag
	if (!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}
	registers[0] = result;
	registers[1] = result >> 8;
	program_counter++;
}

void Call(uint16_t argument_1) {
    printf("CALL 0x%04X \n", argument_1);
	data_memory[stack_pointer--] = ((program_counter + 2) >> 8);
	data_memory[stack_pointer--] = program_counter + 2;
	program_counter = argument_1;
}

void Lsr(uint16_t argument_1) {
    printf("LSR R%d \n", argument_1);

	uint8_t result = registers[argument_1] >> 1;

	// shifting through carry flag
	// v = n xor c, but n is set to 0 so v = c
	// s = n xor v, but n is 0, so s = v = c
	if (registers[argument_1] & 0x01) {
		SetBit(SREG, C_BIT);
		SetBit(SREG, V_BIT);
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, C_BIT);
		ClearBit(SREG, V_BIT);
		ClearBit(SREG, S_BIT);
	}

	// set zero flag
	if (!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	// n flag = 0
	ClearBit(SREG, N_BIT);
	registers[argument_1] = result;
	program_counter++;
}

void Inc(uint16_t argument_1) {
    printf("INC R%d \n", argument_1);
	uint8_t result = registers[argument_1] + 1;

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, ~Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	if(result == 0x80) {
		SetBit(SREG, V_BIT);

	} else {
		ClearBit(SREG, V_BIT);
	}

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Cbi(uint16_t argument_1, uint16_t argument_2) {
    printf("CBI $%d, %d \n", argument_1, argument_2);
	ClearBit(argument_1 + 0x0020, argument_2);
	program_counter++;
}

void Push(uint16_t argument_1) {
    printf("PUSH R%d \n", argument_1);
	data_memory[stack_pointer--] = registers[argument_1];
	program_counter++;
}

void Pop(uint16_t argument_1) {
    printf("POP R%d \n", argument_1);
	registers[argument_1] = data_memory[++stack_pointer];
	program_counter++;
}

void Reti() {
    printf("RETI \n");
	program_counter = data_memory[++stack_pointer];
	program_counter |= (data_memory[++stack_pointer] << 8);
	SetBit(SREG, I_BIT);
}

void Ret() {
    printf("RET \n");
	program_counter = data_memory[++stack_pointer];
	program_counter |= (data_memory[++stack_pointer] << 8);
}

void Sts(uint16_t argument_1, uint16_t argument_2) {
    printf("STS 0x%04X, R%d \n", argument_2, argument_1);
	data_memory[argument_2] = registers[argument_1];
	program_counter = program_counter + 2;
}

void Lds(uint16_t argument_1, uint16_t argument_2) {
    printf("LDS R%d, 0x%04X \n", argument_1, argument_2);
	registers[argument_1] = data_memory[argument_2];
	program_counter = program_counter + 2;
}

void Ori(uint16_t argument_1, uint16_t argument_2) {
    printf("ANDI R%d, 0x%04X \n", argument_1, argument_2);
	uint8_t result = registers[argument_1] | argument_2;

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	ClearBit(SREG, ~V_BIT);

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Subi(uint16_t argument_1, uint16_t argument_2) {
    printf("SUBI R%d, 0x%04X \n", argument_1, argument_2);
	int8_t result = registers[argument_1] - argument_2;

	// half carry flag
	bool Rd3 = GetBit(argument_1, 3);
	bool Rr3 = argument_2 & (1 << 3);
	bool R3 = result & (1 << 3);
	if ((!Rd3 && Rr3) || (Rr3 && R3) || (R3 && !Rd3)) {
		SetBit(SREG, H_BIT);
	} else {
		ClearBit(SREG, H_BIT);
	}

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	bool Rd7 = GetBit(argument_1, 7);
	bool Rr7 = argument_2 & (1 << 7);
	bool R7 = result & (1 << 7);
	if ((Rd7 && !Rr7 && !R7) || (!Rd7 && Rr7 && R7)) {
		SetBit(SREG, V_BIT);
	} else {
		ClearBit(SREG, V_BIT);
	}

	// carry flag
	if((!Rd7 && Rr7) || (Rr7 && R7) || (R7 && !Rd7)) {
		SetBit(SREG, C_BIT);
	} else {
		ClearBit(SREG, C_BIT);
	}

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Adc(uint16_t argument_1, uint16_t argument_2) {
    printf("ADC R%d, R%d \n", argument_1, argument_2);
	uint16_t result = registers[argument_1] + registers[argument_2] + (uint8_t) GetBit(SREG, C_BIT);

	// carry flag
	if(result > 255) {
		SetBit(SREG, C_BIT);
	} else {
		ClearBit(SREG, C_BIT);
	}

	// half carry flag
	bool Rd3 = GetBit(argument_1, 3);
	bool Rr3 = GetBit(argument_2, 3);
	bool R3 = result & (1 << 3);
	if ((Rd3 && Rr3) || (Rr3 && !R3) || (!R3 && Rd3)) {
		SetBit(SREG, H_BIT);
	} else {
		ClearBit(SREG, H_BIT);
	}

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	bool Rd7 = GetBit(argument_1, 7);
	bool Rr7 = GetBit(argument_2, 7);
	bool R7 = result & (1 << 7);
	if ((Rd7 && Rr7 && !R7) || (!Rd7 && Rr7 && R7)) {
		SetBit(SREG, V_BIT);
	} else {
		ClearBit(SREG, V_BIT);
	}

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result % 256;
	program_counter++;
}

void Cp(uint16_t argument_1, uint16_t argument_2) {
    printf("ADC R%d, R%d \n", argument_1, argument_2);
	int8_t result = registers[argument_1] - registers[argument_2];

	// half carry flag
	bool Rd3 = GetBit(argument_1, 3);
	bool Rr3 = GetBit(argument_2, 3);
	bool R3 = result & (1 << 3);
	if ((!Rd3 && Rr3) | (Rr3 && R3) | (R3 && !Rd3)) {
		SetBit(SREG, H_BIT);
	} else {
		ClearBit(SREG, H_BIT);
	}

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	bool Rd7 = GetBit(argument_1, 7);
	bool Rr7 = GetBit(argument_2, 7);
	bool R7 = result & (1 << 7);
	if ((Rd7 && !Rr7 && !R7) || (!Rd7 && Rr7 && R7)) {
		SetBit(SREG, V_BIT);
	} else {
		ClearBit(SREG, V_BIT);
	}

	// carry flag
	if((!Rd7 && Rr7) || (Rr7 && R7) || (R7 && !Rd7)) {
		SetBit(SREG, C_BIT);
	} else {
		ClearBit(SREG, C_BIT);
	}

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	program_counter++;
}

void Sub(uint16_t argument_1, uint16_t argument_2) {
    printf("SUB R%d, R%d \n", argument_1, argument_2);
	int8_t result = registers[argument_1] - registers[argument_2];

	// half carry flag
	bool Rd3 = GetBit(argument_1, 3);
	bool Rr3 = GetBit(argument_2, 3);
	bool R3 = result & (1 << 3);
	if ((!Rd3 && Rr3) || (Rr3 && R3) || (R3 && !Rd3)) {
		SetBit(SREG, H_BIT);
	} else {
		ClearBit(SREG, H_BIT);
	}

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	bool Rd7 = GetBit(argument_1, 7);
	bool Rr7 = GetBit(argument_2, 7);
	bool R7 = result & (1 << 7);
	if ((Rd7 && !Rr7 && !R7) || (!Rd7 && Rr7 && R7)) {
		SetBit(SREG, V_BIT);
	} else {
		ClearBit(SREG, V_BIT);
	}

	// carry flag
	if((!Rd7 && Rr7) || (Rr7 && R7) || (R7 && !Rd7)) {
		SetBit(SREG, C_BIT);
	} else {
		ClearBit(SREG, C_BIT);
	}

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Mov(uint16_t argument_1, uint16_t argument_2) {
    printf("MOV R%d, R%d \n", argument_1, argument_2);
	registers[argument_1] = registers[argument_2];
	program_counter++;
}

void Or(uint16_t argument_1, uint16_t argument_2) {
    printf("OR R%d, R%d \n", argument_1, argument_2);
	uint8_t result = registers[argument_1] | registers[argument_2];

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	ClearBit(SREG, V_BIT);

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Eor(uint16_t argument_1, uint16_t argument_2) {
    printf("EOR R%d, R%d \n", argument_1, argument_2);
	uint8_t result = registers[argument_1] ^ registers[argument_2];

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	ClearBit(SREG, V_BIT);

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void And(uint16_t argument_1, uint16_t argument_2) {
    printf("AND R%d, R%d \n", argument_1, argument_2);
	uint8_t result = registers[argument_1] & registers[argument_2];

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, ~N_BIT);
	}

	// signed overflow flag
	ClearBit(SREG, V_BIT);

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result;
	program_counter++;
}

void Add(uint16_t argument_1, uint16_t argument_2) {
    printf("ADD R%d, R%d \n", argument_1, argument_2);
	uint16_t result = registers[argument_1] + registers[argument_2];

	// carry flag
	if(result > 255) {
		SetBit(SREG, C_BIT);
	} else {
		ClearBit(SREG, C_BIT);
	}

	// half carry flag
	bool Rd3 = GetBit(argument_1, 3);
	bool Rr3 = GetBit(argument_2, 3);
	bool R3 = result & (1 << 3);
	if ((Rd3 && Rr3) || (Rr3 && !R3) || (!R3 && Rd3)) {
		SetBit(SREG, H_BIT);
	} else {
		ClearBit(SREG, H_BIT);
	}

	// zero flag
	if(!result) {
		SetBit(SREG, Z_BIT);
	} else {
		ClearBit(SREG, Z_BIT);
	}

	//	negative flag
	if(result & (1 << 7)) {
		SetBit(SREG, N_BIT);
	} else {
		ClearBit(SREG, N_BIT);
	}

	// signed overflow flag
	bool Rd7 = GetBit(argument_1, 7);
	bool Rr7 = GetBit(argument_2, 7);
	bool R7 = result & (1 << 7);
	if ((Rd7 && Rr7 && !R7) || (!Rd7 && Rr7 && R7)) {
		SetBit(SREG, V_BIT);
	} else {
		ClearBit(SREG, V_BIT);
	}

	// s = n xor v
	if(GetBit(SREG,  N_BIT) ^ GetBit(SREG,  V_BIT)) {
		SetBit(SREG, S_BIT);
	} else {
		ClearBit(SREG, S_BIT);
	}
	registers[argument_1] = result % 256;
	program_counter++;
}

void Unknown() {
	printf("Unknown command! \n");
}
