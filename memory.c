/*
 * memory.c
 *
 * Created: 2018-05-29 13:20:08
 *  Author: CEM
 */
#include "memory.h"

uint16_t program_counter = 0;								// 16-bit Program Counter definition
uint16_t stack_pointer = 1024;										// stack pointer definition

uint16_t program_memory[PROGRAM_MEMORY_SIZE];				// instructions are 16-bit wide
uint8_t data_memory[DATA_MEMORY_SIZE];						// data bus is 8-bit wide
uint8_t *registers = data_memory;							// registers R0-R31 start at address 0x00
uint8_t *in_out_memory = data_memory + IO_REGISTERS_OFFSET;	// the in/out registers are at an offset

void SetBit(uint8_t register_address, uint8_t bit) {		// set bit number 'bit' in register
	data_memory[register_address] |= (1 << bit);
}

void ClearBit(uint8_t register_address, uint8_t bit) {		// clear bit number 'bit' in register
	data_memory[register_address] &= ~(1 << bit);
}

bool GetBit(uint8_t register_address, uint8_t bit) {		// get value of bit number 'bit' in register
	return data_memory[register_address] & (1 << bit);
}

void ClearMemory() {
    memset(data_memory, 0, DATA_MEMORY_SIZE);
    memset(program_memory, 0, PROGRAM_MEMORY_SIZE);
}
