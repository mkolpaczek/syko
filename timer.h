/*
 * timer.h
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "stdint.h"

void TimerHandle();
uint16_t GetPrescaler();

#endif /* TIMER_H_ */
