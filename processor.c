/*
 * processor.c
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */
#include "processor.h"
#include "instructions.h"
#include "memory.h"
#include "gpio.h"

#define MASK_BRNE 0xf401
#define MASK_BRGE 0xf404
#define MASK_BRSH 0xf400
#define MASK_BREQ 0xf001
#define MASK_LDI  0xe000
#define MASK_RCALL 0xd000
#define MASK_OUT 0xb800
#define MASK_ANDI 0x7000
#define MASK_RJMP 0xc000
#define MASK_MUL 0x9c00
#define MASK_CALL 0x940e
#define MASK_LSR 0x9406
#define MASK_INC 0x9403
#define MASK_CBI 0x9800
#define MASK_PUSH 0x920f
#define MASK_POP 0x900f
#define MASK_RETI 0x9518
#define MASK_RET 0x9508
#define MASK_STS 0x9200
#define MASK_LDS 0x9000
#define MASK_ORI 0x6000
#define MASK_SUBI 0x5000
#define MASK_ADC 0x1c00
#define MASK_CP 0x1400
#define MASK_SUB 0x1800
#define MASK_MOV 0x2c00
#define MASK_OR 0x2800
#define MASK_EOR 0x2400
#define MASK_AND 0x2000
#define MASK_ADD 0x0c00

uint64_t cycles_to_execute;					            // number of cycles to execute
bool proceed = true;						            // boolean variable informing the program
                                                        // if we've already simulated all the cycles we wanted

void ProcessorHandle() {
	uint16_t opcode = GetInstruction();                 // read the opcode of current instruction from data memory
	Instruction instruction = Decode(opcode);           // decode the opcode into instruction and arguments
	printf("Program counter = 0x%04X, opcode = 0x%04X, ", program_counter, opcode);
	switch (instruction.type_) {                        // run the specific instruction
		case BRNE:
			Brne(instruction.argument_1_);
			break;

		case BRGE:
			Brge(instruction.argument_1_);
			break;

		case BRSH:
			Brsh(instruction.argument_1_);
			break;

		case BREQ:
			Breq(instruction.argument_1_);
			break;

        case LDI:
            Ldi(instruction.argument_1_, instruction.argument_2_);
            break;

		case RCALL:
			Rcall(instruction.argument_1_);
			break;

		case OUT:
			Out(instruction.argument_1_, instruction.argument_2_);
			break;

		case ANDI:
			Andi(instruction.argument_1_, instruction.argument_2_);
			break;

		case RJMP:
			Rjmp(instruction.argument_1_);
			break;

		case MUL:
			Mul(instruction.argument_1_, instruction.argument_2_);
			break;

		case CALL:
			Call(instruction.argument_1_);
			break;

		case LSR:
			Lsr(instruction.argument_1_);
			break;

		case INC:
			Inc(instruction.argument_1_);
			break;

		case CBI:
			Cbi(instruction.argument_1_, instruction.argument_2_);
			break;

		case PUSH:
			Push(instruction.argument_1_);
			break;

		case POP:
			Pop(instruction.argument_1_);
			break;

		case RETI:
			Reti();
			break;

		case RET:
			Ret();
			break;

		case STS:
			Sts(instruction.argument_1_, instruction.argument_2_);
			break;

		case LDS:
			Lds(instruction.argument_1_, instruction.argument_2_);
			break;

		case ORI:
			Ori(instruction.argument_1_, instruction.argument_2_);
			break;

		case SUBI:
			Subi(instruction.argument_1_, instruction.argument_2_);
			break;

		case ADC:
			Adc(instruction.argument_1_, instruction.argument_2_);
			break;

		case CP:
			Cp(instruction.argument_1_, instruction.argument_2_);
			break;

		case SUB:
			Sub(instruction.argument_1_, instruction.argument_2_);
			break;

		case MOV:
			Mov(instruction.argument_1_, instruction.argument_2_);
			break;

		case OR:
			Or(instruction.argument_1_, instruction.argument_2_);
			break;

		case EOR:
			Eor(instruction.argument_1_, instruction.argument_2_);
			break;

		case AND:
			And(instruction.argument_1_, instruction.argument_2_);
			break;

		case ADD:
			Add(instruction.argument_1_, instruction.argument_2_);
			break;

		case UNKNOWN:
			Unknown();
			break;
	}
	if (++processor_cycles == cycles_to_execute) {
		proceed = false;                                        // stop execution of program after a set number of cycles
	}
}

Instruction Decode(uint16_t opcode) {                           // opcode decoder
	Instruction instruction;

	if((opcode & MASK_BRNE) == MASK_BRNE) {
		instruction.type_ = BRNE;
		instruction.argument_1_ = -(opcode & 0x0200) + ((opcode & 0x01f8) >> 3);
	}
	else if((opcode & MASK_BRGE) == MASK_BRGE) {
		instruction.type_ = BRGE;
		instruction.argument_1_ = -(opcode & 0x0200) + ((opcode & 0x01f8) >> 3);
	}
	else if((opcode & MASK_BRSH) == MASK_BRSH) {
		instruction.type_ = BRSH;
		instruction.argument_1_ = -(opcode & 0x0200) + ((opcode & 0x01f8) >> 3);
	}
	else if((opcode & MASK_BREQ) == MASK_BREQ) {
		instruction.type_ = BREQ;
		instruction.argument_1_ = -(opcode & 0x0200) + ((opcode & 0x01f8) >> 3);
	}
	else if((opcode & MASK_LDI) == MASK_LDI) {
        instruction.type_ = LDI;
        instruction.argument_1_ = ((opcode & 0x00f0) >> 4) + 16;
        instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0f00) >> 4);
	}
	else if((opcode & MASK_RCALL) == MASK_RCALL) {
		instruction.type_ = RCALL;
		instruction.argument_1_ = -(opcode & 0x0800) + (opcode & 0x07ff);
	}
	else if((opcode & MASK_OUT) == MASK_OUT) {
		instruction.type_ = OUT;
		instruction.argument_1_ = (opcode & 0x000f) | ((opcode & 0x0600) >> 5);
		instruction.argument_2_ = (opcode & 0x00f0) >> 4;
	}
	else if((opcode & MASK_ANDI) == MASK_ANDI) {
		instruction.type_ = ANDI;
		instruction.argument_1_ = 16 + ((opcode & 0x00f0) >> 4);
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0f00) >> 4);
	}
	else if((opcode & MASK_RJMP) == MASK_RJMP) {
		instruction.type_ = RJMP;
		instruction.argument_1_ = -(opcode & 0x0800) + (opcode & 0x07ff);
	}
	else if((opcode & MASK_MUL) == MASK_MUL) {
		instruction.type_ = MUL;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_CALL) == MASK_CALL) {
		instruction.type_ = CALL;
		instruction.argument_1_ = GetDirectArgument();
	}
	else if((opcode & MASK_LSR) == MASK_LSR) {
		instruction.type_ = LSR;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
	}
	else if((opcode & MASK_INC) == MASK_INC) {
		instruction.type_ = INC;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
	}
	else if((opcode & MASK_CBI) == MASK_CBI) {
		instruction.type_ = CBI;
		instruction.argument_1_ = (opcode & 0x00f8) >> 3;
		instruction.argument_2_ = (opcode & 0x0007);
	}
	else if((opcode & MASK_PUSH) == MASK_PUSH) {
		instruction.type_ = PUSH;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
	}
	else if((opcode & MASK_POP) == MASK_POP) {
		instruction.type_ = POP;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
	}
	else if((opcode & MASK_RETI) == MASK_RETI) {
		instruction.type_ = RETI;
	}
	else if((opcode & MASK_RET) == MASK_RET) {
		instruction.type_ = RET;
	}
	else if((opcode & MASK_STS) == MASK_STS) {
		instruction.type_ = STS;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = GetDirectArgument();
	}
	else if((opcode & MASK_LDS) == MASK_LDS) {
		instruction.type_ = LDS;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = GetDirectArgument();
	}
	else if((opcode & MASK_ORI) == MASK_ORI) {
		instruction.type_ = ORI;
		instruction.argument_1_ = 16 + ((opcode & 0x00f0) >> 4);
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0f00) >> 4);
	}
	else if((opcode & MASK_SUBI) == MASK_SUBI) {
		instruction.type_ = SUBI;
		instruction.argument_1_ = 16 + ((opcode & 0x00f0) >> 4);
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0f00) >> 4);
	}
	else if((opcode & MASK_ADC) == MASK_ADC) {
		instruction.type_ = ADC;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_CP) == MASK_CP) {
		instruction.type_ = CP;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_SUB) == MASK_SUB) {
		instruction.type_ = SUB;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_MOV) == MASK_MOV) {
		instruction.type_ = MOV;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_OR) == MASK_OR) {
		instruction.type_ = OR;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_EOR) == MASK_EOR) {
		instruction.type_ = EOR;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_AND) == MASK_AND) {
		instruction.type_ = AND;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else if((opcode & MASK_ADD) == MASK_ADD) {
		instruction.type_ = ADD;
		instruction.argument_1_ = (opcode & 0x01f0) >> 4;
		instruction.argument_2_ = (opcode & 0x000f) | ((opcode & 0x0200) >> 5);
	}
	else {
		instruction.type_ = UNKNOWN;
	}
	return instruction;
}

uint16_t GetInstruction() {
	return program_memory[program_counter];                         // read the next instruction from program memory
}

uint16_t GetDirectArgument() {
	return program_memory[program_counter + 1];                     // get the direct argument of an instruction from program memory
}

void CheckForInterrupts() {
	if(!GetBit(SREG, I_BIT)) {										// check for global interrupt enable flag
		return;
	} else {
		if(GetBit(TIFR2, OCFA) && GetBit(TIMSK2, OCIEA)) {									// check for Output Compare A Match Flag
			ClearBit(SREG, I_BIT);									// clear global interrupt enable flag
			ClearBit(TIFR2, OCFA);
			data_memory[stack_pointer--] = (program_counter >> 8);	// push PC onto stack
			data_memory[stack_pointer--] = program_counter;
			program_counter = TIM2_COMPA_VECTOR;					// go to interrupt vector
			return;
		}
		if(GetBit(TIFR2, OCFB) && GetBit(TIMSK2, OCIEB)) {			// check for Output Compare B Match Flag
			ClearBit(SREG, I_BIT);									// clear global interrupt enable flag
			ClearBit(TIFR2, OCFB);
			data_memory[stack_pointer--] = (program_counter >> 8);	// push PC onto stack
			data_memory[stack_pointer--] = program_counter;
			program_counter = TIM2_COMPB_VECTOR;					// go to interrupt vector
			return;
		}
		if(GetBit(TIFR2, TOV) && GetBit(TIMSK2, TOIE)) {		    // check for Timer2 Overflow Flag
			ClearBit(SREG, I_BIT);									// clear global interrupt enable flag
			ClearBit(TIFR2, TOV);
			data_memory[stack_pointer--] = (program_counter >> 8);	// push PC onto stack
			data_memory[stack_pointer--] = program_counter;
			program_counter = TIM2_OVF_VECTOR;						// go to interrupt vector
		}
	}
}

