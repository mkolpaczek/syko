/*
 * files.h
 *
 *  Created on: 27 maj 2018
 *      Author: CEM
 */

#ifndef FILES_H_
#define FILES_H_


void LoadProgramMemory(const char *file);
void LoadDataMemory(const char *file);
void SaveDataMemory(const char *file);

#endif /* FILES_H_ */
