/*
 * processor.h
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */

#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum InstructionType {
	BRNE,
	BRGE,
	BRSH,
	BREQ,
	LDI,
	RCALL,
	OUT,
	ANDI,
	RJMP,
	MUL,
	CALL,
	LSR,
	INC,
	CBI,
	PUSH,
	POP,
	RETI,
	RET,
	//LAS,
	//LAC,
	STS,
	LDS,
	ORI,
	SUBI,
	ADC,
	CP,
	SUB,
	MOV,
	OR,
	EOR,
	AND,
	ADD,
	UNKNOWN
} InstructionType;

typedef struct Instruction {
	InstructionType type_;
	int32_t argument_1_;
	int32_t argument_2_;
} Instruction;


void ProcessorHandle();
Instruction Decode(uint16_t opcode);
uint16_t GetInstruction();
uint16_t GetDirectArgument();
void CheckForInterrupts();

uint64_t processor_cycles;

#endif /* PROCESSOR_H_ */
