/*
 * instructions.h
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */

#ifndef INSTRUCTIONS_H_
#define INSTRUCTIONS_H_

void Brne(int16_t argument_1);
void Brge(int16_t argument_1);
void Brsh(int16_t argument_1);
void Breq(int16_t argument_1);
void Ldi(uint16_t argument_1, uint16_t argument_2);
void Rcall(int16_t argument_1);
void Out(uint16_t argument_1, uint16_t argument_2);
void Andi(uint16_t argument_1, uint16_t argument_2);
void Rjmp(int16_t argument_1);
void Mul(uint16_t argument_1, uint16_t argument_2);
void Call(uint16_t argument_1);
void Lsr(uint16_t argument_1);
void Inc(uint16_t argument_1);
void Cbi(uint16_t argument_1, uint16_t argument_2);
void Push(uint16_t argument_1);
void Pop(uint16_t argument_1);
void Reti();
void Ret();
void Sts(uint16_t argument_1, uint16_t argument_2);
void Lds(uint16_t argument_1, uint16_t argument_2);
void Ori(uint16_t argument_1, uint16_t argument_2);
void Subi(uint16_t argument_1, uint16_t argument_2);
void Adc(uint16_t argument_1, uint16_t argument_2);
void Cp(uint16_t argument_1, uint16_t argument_2);
void Sub(uint16_t argument_1, uint16_t argument_2);
void Mov(uint16_t argument_1, uint16_t argument_2);
void Or(uint16_t argument_1, uint16_t argument_2);
void Eor(uint16_t argument_1, uint16_t argument_2);
void And(uint16_t argument_1, uint16_t argument_2);
void Add(uint16_t argument_1, uint16_t argument_2);
void Unknown();




#endif /* INSTRUCTIONS_H_ */
