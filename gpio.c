#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include "gpio.h"

//Linux compliance
#ifndef O_BINARY
	#define O_BINARY 0
#endif

#ifndef O_RDWR
	#define O_RDWR 0
#endif

#define MAX_LINE_LENGTH 128
#define MAX_GPIO_STEPS  65535

uint16_t lines_read = 0;

GpioEvent gpio_story_in[MAX_GPIO_STEPS];		//utrzymujemy informacje co bedzie sie dzialo z GPIO numer x w kolejnych cyklach

int ReadLine(int file_ptr, char *buffer, int buffer_size){
	//zwraca 0 gdy koniec pliku lub nie mozna czytac
	//w buffer jest zawsze string zakonczony zerem, gdy nie udalo sie przeczytac nic to i tak jest tam string zakonczony zerem
	int p = 0;
	char c;

	memset(buffer, '\0', buffer_size);
	for(;;){
		int n = read(file_ptr, &c, 1);
		if(n < 1){
			buffer[p]='\0';
			return 0;
		}
		if(p == 0){
			if((c == 0x0D)||(c == 0x0A)){
				continue;
			}
		}
		if(n == 0){
			buffer[p] = '\0';
			return 0;
		}
		buffer[p++] = c;
		if(p > buffer_size){
			buffer[buffer_size - 1]='\0';
			return buffer_size;
		}
		if((c == 0x0D)||(c == 0x0A)){
			buffer[p - 1]='\0';
			return p - 1;
		}
	}
	return 0;
}

void LoadGpioChanges(const char *file){
	int file_ptr;
	int event_time;
	int new_gpio_state;
	char read_line[MAX_LINE_LENGTH];
	file_ptr = open(file, O_RDWR, 0);
	if(file_ptr < 0){
		printf("GPIO file not found (%s)!\n", file);
		exit(-1);
	}

	memset(gpio_story_in, 0xff, sizeof(gpio_story_in));

	for(;;){
		if (ReadLine(file_ptr, read_line, MAX_LINE_LENGTH) <= 0) {
			break;
		}
		if(strlen(read_line) == 0 || read_line[0] == '#') {
			continue;
		}
		int temp = file_ptr;
		sscanf(read_line, "%d,%d", &event_time, &new_gpio_state);
		file_ptr = temp;
		gpio_story_in[lines_read].time_ = event_time;
		gpio_story_in[lines_read].new_state_ = new_gpio_state;
		lines_read++;
	}
	close(file_ptr);
}

bool GetGpioChange(uint16_t step_number) {
	uint16_t i = 0;
	while((i < lines_read) && (gpio_story_in[i].time_ < step_number)) {
		i++;
	}
	if(i == lines_read) {
        return false;
	}
	if ((step_number == gpio_story_in[i].time_ ) && (gpio_story_in[i].new_state_ == true)) {
		return true;
	} else {
		return false;
	}
}
