/*
 * memory.h
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */

#ifndef MEMORY_H_
#define MEMORY_H_

#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"


#define PROGRAM_MEMORY_SIZE 	2048
#define DATA_MEMORY_SIZE 		2048
#define IO_REGISTERS_OFFSET 	0x0020

// interrupt vector table
#define TIM2_COMPA_VECTOR 		0x000E 								// Timer2 CompareA
#define TIM2_COMPB_VECTOR 		0x0010 								// Timer2 CompareB
#define TIM2_OVF_VECTOR 		0x0012 								// Timer2 Overflow

// register addresses definitions
#define SREG 	0x5f												// status register
#define TCCR2B 	0xB1												// Timer2 Control Register B
#define TCNT2 	0xB2												// Timer2 Counter Value Register
#define OCR2A 	0xB3												// Timer2 Output Compare Register A
#define OCR2B 	0xB4												// Timer2 Output Compare Register B
#define TIMSK2 	0x70												// Timer2 Interrupt Mask Register
#define TIFR2 	0x37												// Timer2 Interrupt Flag Register
#define ASSR    0xB6                                                // Timer2 Asynchronous Status Register

// SREG bits definitions
#define C_BIT   0													// Carry Flag
#define Z_BIT   1													// Zero Flag
#define N_BIT   2													// Negative Flag
#define V_BIT   3													// Two�s Complement Overflow Flag
#define S_BIT   4													// Sign Flag, S = N xor V
#define H_BIT   5													// Half Carry Flag
#define T_BIT   6													// Copy Storage
#define I_BIT   7													// Global Interrupt Enable

// TIMSK2 bits definitions
#define TOIE    0													// Timer 2 Overflow Interrupt Enable
#define OCIEA   1													// Output Compare A Match Interrupt Enable
#define OCIEB   2													// Output Compare B Match Interrupt Enable

// TIFR2 bits definitions
#define TOV     0													// Timer2 Overflow Flag
#define OCFA    1													// Output Compare A Match Flag
#define OCFB    2													// Output Compare B Match Flag

// ASSR bits definition
#define AS2   5

extern uint16_t program_counter;									// 16-bit Program Counter definition
extern uint16_t stack_pointer;										// stack pointer definition

extern uint16_t program_memory[PROGRAM_MEMORY_SIZE];				// instructions are 16-bit wide
extern uint8_t data_memory[DATA_MEMORY_SIZE];						// data bus is 8-bit wide
extern uint8_t *registers;											// registers R0-R31 start at address 0x00
extern uint8_t *in_out_memory;										// in/out registers start at 0x0020

void SetBit(uint8_t register_address, uint8_t bit);					// set bit number 'bit' in register
void ClearBit(uint8_t register_address, uint8_t bit);				// clear bit number 'bit' in register
bool GetBit(uint8_t register_address, uint8_t bit);					// get value of bit number 'bit' in register
void ClearMemory();

#endif /* MEMORY_H_ */
