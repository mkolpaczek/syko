/*
 * timer.c
 *
 *  Created on: 11 maj 2018
 *      Author: CEM
 */


#include "timer.h"
#include "memory.h"
#include "gpio.h"
#include "processor.h"


uint16_t prescaler_count = 0;								// variable storing clock cycles since last timer tick

void TimerHandle() {
	uint16_t prescaler_value = GetPrescaler();
	if (prescaler_value) {
        if (GetBit(ASSR, AS2)) {                            // check if the timer is set to external source
            if (GetGpioChange(processor_cycles)) {
                prescaler_count++;
            } else {
                return;
            }
        } else {
            prescaler_count++;
        }

		if(prescaler_count == prescaler_value){
			prescaler_count = 0;

			data_memory[TCNT2] = (data_memory[TCNT2] + 1);

			if(!data_memory[TCNT2]) {
				SetBit(TIFR2, TOV);							// Set the Timer2 Overflow Flag
			}

			if(data_memory[TCNT2] == data_memory[OCR2A]) {
				SetBit(TIFR2, OCFA);						// Set the Timer2 Output Compare A Match Flag
			}

			if(data_memory[TCNT2] == data_memory[OCR2B]) {
				SetBit(TIFR2, OCFB);						// Set the Timer2 Output Compare B Match Flag
			}
		}

	}
}

uint16_t GetPrescaler() {
	uint16_t temp = data_memory[TCCR2B] & 0x07;				// check the value of last 3 TCCR2B bits
	switch (temp) {
		case 0:
			return 0;
			break;

		case 1:
			return 1;
			break;

		case 2:
			return 8;
			break;

		case 3:
			return 32;
			break;

		case 4:
			return 64;
			break;

		case 5:
			return 128;
			break;

		case 6:
			return 256;
			break;

		case 7:
			return 1024;
			break;
	}
	return 0;
}
