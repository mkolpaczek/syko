#include "processor.h"
#include "instructions.h"
#include "memory.h"
#include "timer.h"
#include "files.h"
#include "gpio.h"

const char* kDataInputName = "file_data_in.bin";
const char* kDataOutputName = "file_data_out.bin";
const char* kCodeInputName = "file_code.bin";
const char* kGpioInputName = "gpio_in.txt";

extern bool proceed;
extern uint64_t cycles_to_execute;

int main(int argc, char **argv) {
    ClearMemory();
    processor_cycles = 0;
	LoadProgramMemory(kCodeInputName);
	LoadDataMemory(kDataInputName);
	LoadGpioChanges(kGpioInputName);
	if (argc > 1) {
		cycles_to_execute = strtoul(argv[1], NULL, 10);
	} else {
        cycles_to_execute = 1024;
	}
	while (proceed) {
		TimerHandle();
		CheckForInterrupts();
		ProcessorHandle();
	}
	SaveDataMemory(kDataOutputName);

}
